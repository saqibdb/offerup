//
//  CityModel.swift
//  OfferUp
//
//  Created by iBuildX on 03/10/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

import UIKit
import HandyJSON

class CityModel: HandyJSON {
    var lng: String!
    var country: String!
    var lat: String!
    var name: String!

    required init() {}
}

/*
{
    "lng" : "-87.77305",
    "country" : "US",
    "lat" : "30.88296",
    "name" : "Bay Minette"
}
*/

