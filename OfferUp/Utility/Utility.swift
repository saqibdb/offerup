//
//  Utility.swift
//  uChoose
//
//  Created by iBuildX on 09/07/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

import UIKit
import Alamofire


class Utility: NSObject {
    
    typealias ResponseHandler = (_ success:Bool, _ error : String? , _ response : [OfferUpCarModel]?) -> Void
    typealias ProgressHandler = (_ progressStr : Double?) -> Void

    static let baseUrl = "https://offerup.com/webapi/search/v2/feed/?q="


    class func getCars(withSearchedText searchedText : String, andWithCity city : CityModel ,andResponseHandler responseHandler : @escaping ResponseHandler, andProgressHandler progressHandler : @escaping ProgressHandler) {
        
        let escapedString = searchedText.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        print(escapedString!)
        
        let searchParams = "&limit=1000&radius=50&platform=web&experiment_id=experimentmodel24&lon=\(city.lng!)&lat=\(city.lat!)&sort=-posted"
        
        

        
        
        let proxyHost = "212.129.48.100"

        let proxyPort = NSNumber(integerLiteral: 8080)
        let userName = "umer123.offerup_1"
        let password = "umer1234"
        
        
        
        let proxyDict : [String : Any] = ["HTTPSEnable" : NSNumber(integerLiteral: 1),
                                          kCFStreamPropertyHTTPSProxyHost as String: proxyHost,
                                           kCFStreamPropertyHTTPSProxyPort as String: proxyPort,
                                           
                                           "HTTPEnable" : NSNumber(integerLiteral: 1),
                                           kCFStreamPropertyHTTPProxyHost as String: proxyHost,
                                           kCFStreamPropertyHTTPProxyPort as String: proxyPort,
                                           kCFProxyUsernameKey as String: userName,
                                           kCFProxyPasswordKey as String: password,
                                           ]
        
        
        let configuration = URLSessionConfiguration.default
        configuration.connectionProxyDictionary = proxyDict
        configuration.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData

        let headers : [String : String] = ["content-type": "application/json",
        "cache-control": "no-cache",
        "postman-token": "5bc2aa35-08c9-a436-905d-5d801e18feff",
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36"]
        var request = URLRequest(url: URL(string: "\(baseUrl)\(escapedString!)\(searchParams)")!,timeoutInterval: Double.infinity)

        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.allHTTPHeaderFields = headers

        request.httpMethod = "GET"
        
        
        let session = URLSession(configuration: configuration, delegate: nil, delegateQueue: OperationQueue.main)
    

        let task = session.dataTask(with: request) { data, response, error in
          guard let data = data else {
            
            responseHandler(false , error?.localizedDescription , nil)
            return
          }
            if let JSON = String(data: data, encoding: .utf8)!.toDictionary(){
                converJsonToModels(withJson: JSON as! [String : Any], andResponseHandler: responseHandler)
            }else{
                responseHandler(false , "Please Check VPN." , nil)
            }
        }

        task.resume()
        
        /*
        
        let request = URLRequest(url: URL(string: "\(baseUrl)\(escapedString!)\(searchParams)")!)
        
        let configuration = URLSessionConfiguration.default
        configuration.requestCachePolicy = URLRequest.CachePolicy.reloadIgnoringLocalCacheData
        configuration.httpAdditionalHeaders = ["Proxy-Authorization":  Request.authorizationHeader(user: "umer123.offerup_1", password: "umer1234")! ]

        
        
        
        //configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
        Alamofire.request(request).downloadProgress(closure: { (progress) in
            progressHandler(progress.fractionCompleted)
        }).responseString(completionHandler: { (response) in
            switch response.result {
            case .success :
                
                if let JSON = (response.result.value)!.toDictionary(){
                    converJsonToModels(withJson: JSON as! [String : Any], andResponseHandler: responseHandler)
                }else{
                    responseHandler(false , "Please Check VPN." , nil)
                }
            case .failure :
                
                print("ERROR = \((response.error?.localizedDescription)!)")
                
                responseHandler(false , response.error?.localizedDescription , nil)
            }
        })
            */
            /*.responseJSON { response in
                switch response.result {
                case .success :
                    if let JSON = response.result.value as? [String:Any] {
                        converJsonToModels(withJson: JSON, andResponseHandler: responseHandler)
                    }else{
                        responseHandler(false , "Cannot Connect with Server! Please Check Internet." , nil)
                    }
                case .failure :
                    
                    print("ERROR = \((response.error?.localizedDescription)!)")
                    
                    responseHandler(false , response.error?.localizedDescription , nil)
                }
        }*/
    }
    
    class func converJsonToModels(withJson JSON : [String:Any], andResponseHandler responseHandler : @escaping ResponseHandler){
        var allFoundCars :[OfferUpCarModel] = [OfferUpCarModel]()

        if let statusDict = JSON["status"]! as? [String:Any] {
            if let statusMessageDict = statusDict["message"]! as? String {
                if statusMessageDict != "success" {
                    responseHandler(false , statusMessageDict , nil)
                    return
                }
            }
            else{
                responseHandler(false , "No Status from server" , nil)
                return
            }
        }
        else{
            responseHandler(false , "No Status from server" , nil)
            return
        }

        //message
        
        if let dataDict = JSON["data"]! as? [String:Any] {
            if let feedItemDicts = dataDict["feed_items"]! as? [[String:Any]] {
                for feedItemDict in feedItemDicts {
                    let newOfferUpCarModel = OfferUpCarModel.deserialize(from: feedItemDict)
                    if newOfferUpCarModel != nil {
                        allFoundCars.append(newOfferUpCarModel!)
                    }
                }
                responseHandler(true , nil , allFoundCars)
            }
            else{
                responseHandler(false , "Issue in Data Reading." , nil)
            }
        }
        else{
            responseHandler(false , "Issue in Data Reading." , nil)
        }
    }
}



extension String{
    func toDictionary() -> NSDictionary! {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary
            } catch {
                print(error.localizedDescription)
                return nil
            }
        }
        return nil
    }
}
