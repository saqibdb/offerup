//
//  OfferUpCarModel.swift
//  OfferUp
//
//  Created by iBuildX on 01/10/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

import UIKit
import HandyJSON


class OfferUpCarModel: HandyJSON {

    var type: String!
    var item: OfferUpItemModel!
    
    
    var isAlreadyOpened : Bool = false
    var isVeryNew : Int = 12

    
    

    required init() {}
}

class OfferUpItemModel: HandyJSON {
    
    var post_date: Date = Date()
    var id: String!
    var location_name: String!
    var title: String!
    var post_date_ago: String!
    var get_full_url: String!
    var post_from_store_address: String!

    
    
    
    var description: String!
    var price: String!

    var owner: OfferUpOwnerModel!
    var vehicle_attributes: OfferUpVehicleModel!
    
    
    func mapping(mapper: HelpingMapper) {
        mapper <<<
            post_date <-- CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    }
    

    required init() {}
}

class OfferUpOwnerModel: HandyJSON {
    
    var first_name: String!

    required init() {}
}

class OfferUpVehicleModel: HandyJSON {
    
    var vehicle_id: String!
    var vehicle_make: String!
    var vehicle_miles: String!
    var vehicle_model: String!
    var vehicle_style_display: String!
    var vehicle_year: String!

    required init() {}
}




/*
"type":"item",
"item":{
    "distance":15,
    "get_img_medium_height":225,
    "post_date":"2018-09-26T16:45:40.969Z","yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    "get_img_medium_width":300,
    "owner":{
        "first_name":"Saad",
        "get_profile":{
            "rating":{
                "count":4,
                "average":4.5
            },
            "verified":0,
            "avatar_normal":"https://images.offerup.com/IiZicMMybJZB_r4ddj_KWIkmrKU=/300x0/smart/ac78/o39000995_12834.jpg",
            "avatar_square":"https://images.offerup.com/spYmZP2eLCS6gPQh4J9sSc9KAqY=/100x100/smart/ac78/o39000995_12834.jpg",
            "public_location_name":"Reston, VA",
            "not_active":false,
            "uses_default_avatar":false
        },
        "id":39000995,
        "identity_attributes":{
            "is_truyou_member":false,
            "autos_dealer_payment_info_on_file":false,
            "is_autos_dealer":false,
            "is_small_business":false,
            "potential_autos_seller":false
        },
        "date_joined":"2017-12-29T22:32:07.671Z",
        "softBlocked":false,
        "active":false
    },
    "watched":false,
    "get_img_small_width":140,
    "id":552178636,
    "category":{
        "id":19,
        "name":"Cars & Trucks"
    },
    "location_name":"Reston, VA",
    "get_img_small_height":105,
    "title":"2004 Toyota Highlander, AWD, 3 Row Seats",
    "post_date_ago":"2 days",
    "get_full_url":"https://offerup.com/item/detail/552178636/",
    "priority":100,
    "state":3,
    "longitude":-77.3705,
    "latitude":38.9383,
    "get_img_permalink_medium":"https://images.offerup.com/ytCIRgxOcGVui9CsF-rvLyA4-o0=/300x225/6585/6585a23869464c1da664aa2dc96cf778.jpg",
    "sort_label":"Items near Manassas",
    "description":"Normal wear and tear. NO accidents. Clean title. Has only 131K miles. Timing belt, steering bet, and water pump replaced at 106K. Michelin Tires are like new. Serious buyers only. ",
    "paid":false,
    "payable":false,
    "image_mob_det_hd":"https://images.offerup.com/ytCIRgxOcGVui9CsF-rvLyA4-o0=/300x225/6585/6585a23869464c1da664aa2dc96cf778.jpg",
    "image_mob_list_hd":"https://images.offerup.com/MCp5rAmAa55p5Ka4836MtF9_LGw=/140x105/6585/6585a23869464c1da664aa2dc96cf778.jpg",
    "listing_type":2,
    "condition":40,
    "post_from_store_address":"Reston, VA",
    "photos":[
    {
    "uuid":"6585a23869464c1da664aa2dc96cf778",
    "images":{
    "detail_full":{
    "url":"https://images.offerup.com/3PVDpPCDfgtg9Tus-m52VmvoUcA=/600x450/6585/6585a23869464c1da664aa2dc96cf778.jpg",
    "width":600,
    "height":450
    },
    "detail":{
    "url":"https://images.offerup.com/ytCIRgxOcGVui9CsF-rvLyA4-o0=/300x225/6585/6585a23869464c1da664aa2dc96cf778.jpg",
    "width":300,
    "height":225
    },
    "list":{
    "url":"https://images.offerup.com/MCp5rAmAa55p5Ka4836MtF9_LGw=/140x105/6585/6585a23869464c1da664aa2dc96cf778.jpg",
    "width":140,
    "height":105
    }
    }
    },
    {
    "uuid":"1887127bdd90482fbefa44a6db8320d3",
    "images":{
    "detail_full":{
    "url":"https://images.offerup.com/6g9Dtm1lsbuU-NZ2pgg_UUmZd9c=/600x450/1887/1887127bdd90482fbefa44a6db8320d3.jpg",
    "width":600,
    "height":450
    },
    "detail":{
    "url":"https://images.offerup.com/kmtZPA5xXRhtua639SQMeQd1ExM=/300x225/1887/1887127bdd90482fbefa44a6db8320d3.jpg",
    "width":300,
    "height":225
    },
    "list":{
    "url":"https://images.offerup.com/CUtehJi9jfLwhITY2njR7yArrbU=/140x105/1887/1887127bdd90482fbefa44a6db8320d3.jpg",
    "width":140,
    "height":105
    }
    }
    },
    {
    "uuid":"12b6dc19f6d443f0ba25a5803dd776d9",
    "images":{
    "detail_full":{
    "url":"https://images.offerup.com/6gqxM6x8n1yYbCnsKddfIJxkFTE=/600x450/12b6/12b6dc19f6d443f0ba25a5803dd776d9.jpg",
    "width":600,
    "height":450
    },
    "detail":{
    "url":"https://images.offerup.com/-cjDsjEfGdXQsHBXMcM0oKZ2e7U=/300x225/12b6/12b6dc19f6d443f0ba25a5803dd776d9.jpg",
    "width":300,
    "height":225
    },
    "list":{
    "url":"https://images.offerup.com/JaqtUwiTmVCcj7dI-sjNXV5bZ68=/140x105/12b6/12b6dc19f6d443f0ba25a5803dd776d9.jpg",
    "width":140,
    "height":105
    }
    }
    },
    {
    "uuid":"a0cd799af9394c66a9823cd6a18cf3d8",
    "images":{
    "detail_full":{
    "url":"https://images.offerup.com/pT7hhMBrKdDBMVK_uG-iP70jRmw=/600x450/a0cd/a0cd799af9394c66a9823cd6a18cf3d8.jpg",
    "width":600,
    "height":450
    },
    "detail":{
    "url":"https://images.offerup.com/dgTIs-2So_CVZMu3xQXD6CFuh1k=/300x225/a0cd/a0cd799af9394c66a9823cd6a18cf3d8.jpg",
    "width":300,
    "height":225
    },
    "list":{
    "url":"https://images.offerup.com/_DFXrXZCmTSlumhvnXbP0fQFxD0=/140x105/a0cd/a0cd799af9394c66a9823cd6a18cf3d8.jpg",
    "width":140,
    "height":105
    }
    }
    },
    {
    "uuid":"5c2d2fade2894741b3cea890df5f7b6b",
    "images":{
    "detail_full":{
    "url":"https://images.offerup.com/DTnY2psJdhHfzIhD2xwClhHcfBk=/600x450/5c2d/5c2d2fade2894741b3cea890df5f7b6b.jpg",
    "width":600,
    "height":450
    },
    "detail":{
    "url":"https://images.offerup.com/_y_D_wLThYWudyH4CmWJiMzJRHk=/300x225/5c2d/5c2d2fade2894741b3cea890df5f7b6b.jpg",
    "width":300,
    "height":225
    },
    "list":{
    "url":"https://images.offerup.com/HYveowwk2EfWcufg2BLj5AEjf9U=/140x105/5c2d/5c2d2fade2894741b3cea890df5f7b6b.jpg",
    "width":140,
    "height":105
    }
    }
    },
    {
    "uuid":"85261934002e40e9b19f89002f29be8f",
    "images":{
    "detail_full":{
    "url":"https://images.offerup.com/LVV-LkosVdmrO8fv5icdYkXeelE=/600x450/8526/85261934002e40e9b19f89002f29be8f.jpg",
    "width":600,
    "height":450
    },
    "detail":{
    "url":"https://images.offerup.com/cgtLp_7O3do3M6sCDeZvmTKeffc=/300x225/8526/85261934002e40e9b19f89002f29be8f.jpg",
    "width":300,
    "height":225
    },
    "list":{
    "url":"https://images.offerup.com/wysfGRv73CENYncW6BRLgI5sEIA=/140x105/8526/85261934002e40e9b19f89002f29be8f.jpg",
    "width":140,
    "height":105
    }
    }
    },
    {
    "uuid":"6cd1226813534a7aa287842896b5a3fd",
    "images":{
    "detail_full":{
    "url":"https://images.offerup.com/bz2gg9lam6pos_PxnHLb0czVbp8=/600x800/6cd1/6cd1226813534a7aa287842896b5a3fd.jpg",
    "width":600,
    "height":800
    },
    "detail":{
    "url":"https://images.offerup.com/SzA-03xk8SQJ2q2LquY4K39td9g=/300x400/6cd1/6cd1226813534a7aa287842896b5a3fd.jpg",
    "width":300,
    "height":400
    },
    "list":{
    "url":"https://images.offerup.com/U4PvizcYAloJYNIv-sUzJPtVhfQ=/140x186/6cd1/6cd1226813534a7aa287842896b5a3fd.jpg",
    "width":140,
    "height":186
    }
    }
    },
    {
    "uuid":"84bed2e67332403cab1c50af61744ea3",
    "images":{
    "detail_full":{
    "url":"https://images.offerup.com/xEFVnXAZkQPovGDMs95MJ4PT71s=/600x450/84be/84bed2e67332403cab1c50af61744ea3.jpg",
    "width":600,
    "height":450
    },
    "detail":{
    "url":"https://images.offerup.com/WuThl-YCbH_WNW0U1MRW-ijeXes=/300x225/84be/84bed2e67332403cab1c50af61744ea3.jpg",
    "width":300,
    "height":225
    },
    "list":{
    "url":"https://images.offerup.com/q_6t-8clKv6c5HKNKVzby0IoLKg=/140x105/84be/84bed2e67332403cab1c50af61744ea3.jpg",
    "width":140,
    "height":105
    }
    }
    },
    {
    "uuid":"6e6e310cbaae40469ff386746a516c05",
    "images":{
    "detail_full":{
    "url":"https://images.offerup.com/LYudwfygxKNLlSIlQtsXuB382RU=/600x450/6e6e/6e6e310cbaae40469ff386746a516c05.jpg",
    "width":600,
    "height":450
    },
    "detail":{
    "url":"https://images.offerup.com/Pijw4fFixAJNoGZLzkkdBuWyjBo=/300x225/6e6e/6e6e310cbaae40469ff386746a516c05.jpg",
    "width":300,
    "height":225
    },
    "list":{
    "url":"https://images.offerup.com/gHGCxRhkXz31wNQ8TRGt0-uheIM=/140x105/6e6e/6e6e310cbaae40469ff386746a516c05.jpg",
    "width":140,
    "height":105
    }
    }
    },
    {
    "uuid":"652dce2f4f864dcd93e090567c0d0ab0",
    "images":{
    "detail_full":{
    "url":"https://images.offerup.com/ykgolb83igOyiR-Uh10IjHP3URg=/600x450/652d/652dce2f4f864dcd93e090567c0d0ab0.jpg",
    "width":600,
    "height":450
    },
    "detail":{
    "url":"https://images.offerup.com/06q-WdjZZsApSXKVTjWkCVDMtAU=/300x225/652d/652dce2f4f864dcd93e090567c0d0ab0.jpg",
    "width":300,
    "height":225
    },
    "list":{
    "url":"https://images.offerup.com/xymhPvrq74PRmuqHvnEcfkWxhZo=/140x105/652d/652dce2f4f864dcd93e090567c0d0ab0.jpg",
    "width":140,
    "height":105
    }
    }
    }
    ],
    "get_img_permalink_small":"https://images.offerup.com/MCp5rAmAa55p5Ka4836MtF9_LGw=/140x105/6585/6585a23869464c1da664aa2dc96cf778.jpg",
    "get_img_permalink_large":"https://images.offerup.com/3PVDpPCDfgtg9Tus-m52VmvoUcA=/600x450/6585/6585a23869464c1da664aa2dc96cf778.jpg",
    "price":"5000.00",
    "shipping_attributes":{
        "shipping_enabled":false,
        "shipping_price":0.0,
        "show_as_shipped":false,
        "can_ship_to_buyer":false,
        "buy_it_now_enabled":false,
        "seller_pays_shipping":false
    },
    "generic_attributes":null,
    "vehicle_attributes":{
        "vehicle_id":"JTEEP21A40_CD9BA0EC",
        "vehicle_make":"Toyota",
        "vehicle_miles":"2004",
        "vehicle_model":"Highlander",
        "vehicle_style_display":"Base SUV (4-Door)Automatic, All Wheel Drive, ",
        "vehicle_year":"131000"
    },
    "visible":true
},
"tile_id":"ec721d6a-b340-44b2-826b-e2bbe16364b5"
*/
