//
//  ViewController.swift
//  OfferUp
//
//  Created by iBuildX on 28/09/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

import UIKit
import SwiftSpinner
import SwiftEntryKit
import AudioToolbox
import UserNotifications
import Alamofire



class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, URLSessionDelegate {


    
    
    
    
    
    @IBOutlet weak var carSearchText: UITextField!
    
    @IBOutlet weak var goBtn: UIButton!
    
    @IBOutlet weak var carTableView: UITableView!
    
    @IBOutlet weak var timerBtn: UIButton!
    
    @IBOutlet weak var citiesBtn: UIButton!
    
    
    
    
    @IBOutlet weak var carProgressView: UIProgressView!
    
    
    var allFoundCars :[OfferUpCarModel] = [OfferUpCarModel]()
    
    
    var timerValue : Int = 20
    
    var selectedCities : [CityModel] = [CityModel]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        carTableView.dataSource = self
        carTableView.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let bgTask = BackgroundTask()
        bgTask.startBackgroundTasks(2, target: self, selector: #selector(backgroundCallback))
        
        citiesBtn.setTitle("\(selectedCities.count) Cities", for: .normal)
    }
    
    @objc func backgroundCallback(info: Any!) {
        print("BACKGROUND HAPPENING")
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func goAction(_ sender: UIButton) {
        if self.carSearchText.text?.count == 0 {
            AlertsPopUps.showPopupMessage(title: "Error", titleColor: .white, description: "Search Text is Empty", descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
            return
        }
        if self.selectedCities.count == 0 {
            AlertsPopUps.showPopupMessage(title: "Error", titleColor: .white, description: "No Cities has been selected. Please Select atleast 1 city for searching.", descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
            return
        }
        self.view.endEditing(true)
        SwiftSpinner.show("Getting Cars...", animated: true)
        allFoundCars.removeAll()
        getOfferUpCars()
    }
    
    @IBAction func timerAction(_ sender: UIButton) {
        showAlertView()
    }
    
    @IBAction func citiesAction(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: "ToCities", sender: self)
        
    }
    
    @IBAction func testAction(_ sender: UIButton) {
        //self.doRequest()
        self.performSegue(withIdentifier: "toTest", sender: self)
    }
    
    
    
    
    func getOfferUpCars(){
        print("CALLED")
        self.carProgressView.progress = 0.0
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        var checker = 0
        var errorChecker = false
        var errorDescription = ""
        var allCars : [OfferUpCarModel] = [OfferUpCarModel]()
        
        
        for city in selectedCities {
            checker = checker + 1
            Utility.getCars(withSearchedText: self.carSearchText.text!, andWithCity: city, andResponseHandler: { (status, error, response) in
                checker = checker - 1
                self.carProgressView.progress = Float(1/self.selectedCities.count)
                if status == true {
                    for newCar in response! {
                        if newCar.item != nil {
                            if !allCars.contains(where: {($0.item.id == newCar.item.id)}){
                                allCars.append(newCar)
                            }
                        }
                        
                    }
                }
                else{
                    errorChecker = true
                    errorDescription = error!
                }
                if checker == 0 {
                    SwiftSpinner.hide()
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    
                    
                    var newCarsFound = 0
                    for newCar in allCars {
                        if !self.allFoundCars.contains(where: {($0.item.id == newCar.item.id)}){
                            self.allFoundCars.append(newCar)
                            AudioServicesPlaySystemSound(1315)
                            newCarsFound = newCarsFound + 1
                        }
                        else{
                            let oldCar = self.allFoundCars[self.allFoundCars.index(where: {($0.item.id == newCar.item.id)})!]
                            if oldCar.isVeryNew != 0 {
                                oldCar.isVeryNew = oldCar.isVeryNew - 1
                            }
                        }
                    }
                    if newCarsFound > 0 {
                        AlertsPopUps.showPopupMessage(title: "New Cars!", titleColor: .white, description: "\(newCarsFound) new cars has been found.", descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
                        
                        
                        let content = UNMutableNotificationContent()
                        
                        content.title = "New Cars!"
                        content.body = "\(newCarsFound) new cars has been found."
                        content.sound = UNNotificationSound.default()
                        content.badge = NSNumber(value: 1)
                        // Deliver the notification in five seconds.
                        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 0.1, repeats: false)
                        let request = UNNotificationRequest.init(identifier: "FiveSecond", content: content, trigger: trigger)
                        
                        // Schedule the notification.
                        let center = UNUserNotificationCenter.current()
                        center.add(request) { (error) in
                            print(error ?? "")
                        }
                    }
                    self.allFoundCars = self.allFoundCars.sorted(by: { $0.isVeryNew < $1.isVeryNew })
                    self.allFoundCars = self.allFoundCars.sorted(by: { $0.item.post_date > $1.item.post_date })
                    
                    
                    /*
 
 
                     carsForSaleCars = [[carsForSaleCars sortedArrayUsingComparator:^NSComparisonResult(CarsDotComModel *a, CarsDotComModel *b) {
                     return a.listingId<b.listingId;
                     }] mutableCopy];
                     
                     carsForSaleCars = [[carsForSaleCars sortedArrayUsingComparator:^NSComparisonResult(CarsDotComModel *a, CarsDotComModel *b) {
                     return [a.isVeryNew intValue]<[b.isVeryNew intValue];
                     }] mutableCopy];
 */
                    
                    
                    

                    self.carTableView.reloadData()
                    
                    
                    if errorChecker == true {
                        AlertsPopUps.showPopupMessage(title: "Error", titleColor: .white, description: errorDescription, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
                        AudioServicesPlaySystemSound(1315)
                        let content = UNMutableNotificationContent()
                        
                        content.title = "Error!"
                        content.body = errorDescription
                        content.sound = UNNotificationSound.default()
                        content.badge = NSNumber(value: 1)
                        // Deliver the notification in five seconds.
                        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 0.1, repeats: false)
                        let request = UNNotificationRequest.init(identifier: "FiveSecond", content: content, trigger: trigger)
                        
                        // Schedule the notification.
                        let center = UNUserNotificationCenter.current()
                        center.add(request) { (error) in
                            print(error ?? "")
                        }
                    }
                    
                    
                    
                    let sec = Double(self.timerValue)
                    let time = DispatchTime.now() + sec
                    DispatchQueue.main.asyncAfter(deadline: time, execute: {
                        self.getOfferUpCars()
                    })
                }
                
            }) { (progress) in

            }  
        }
    }
    
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return allFoundCars.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "MainTableViewCell") as! MainTableViewCell
        
        let car = allFoundCars[indexPath.row]
        cell.priceText.text = car.item.price
        cell.carNameText.text = car.item.title
        cell.phoneText.text = car.item.owner.first_name

        if car.item.vehicle_attributes != nil {
            cell.makeText.text = car.item.vehicle_attributes.vehicle_make
            cell.modelText.text = car.item.vehicle_attributes.vehicle_model
            cell.carTrimText.text = car.item.vehicle_attributes.vehicle_style_display
            cell.mileageText.text = car.item.vehicle_attributes.vehicle_miles
        }
        else{
            print("ISSUE IN vehicle_attributes")
            cell.makeText.text = ""
            cell.modelText.text = ""
            cell.carTrimText.text = ""
            cell.mileageText.text = ""
        }
        cell.emailText.text = car.item.post_from_store_address
        
        if car.isAlreadyOpened == true {
            cell.topIndicatorview.backgroundColor = .lightGray
            cell.bottomIndicatorView.backgroundColor = .lightGray
            cell.carNameText.textColor = .black
        }
        else{
            if car.isVeryNew > 1 {
                cell.carNameText.textColor = .red
                cell.topIndicatorview.backgroundColor = .red
                cell.bottomIndicatorView.backgroundColor = .red
            }
            else{
                cell.carNameText.textColor = .black
                cell.topIndicatorview.backgroundColor = .black
                cell.bottomIndicatorView.backgroundColor = .black
            }
        }
        cell.newlyListedText.isHidden = true
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let car = allFoundCars[indexPath.row]
        car.isAlreadyOpened = true
        self.performSegue(withIdentifier: "ToDetails", sender: car)
        tableView.deselectRow(at: indexPath, animated: true)
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if sender is OfferUpCarModel {
            let vc = segue.destination as! DetailViewController
            vc.selectedCar = sender as! OfferUpCarModel
        }
        if segue.destination is CitiesViewController {
            let vc = segue.destination as! CitiesViewController
            vc.selectedCities = self.selectedCities
        }
    }
    
    
    
    // MARK: - Others
    
    func showAlertView() {
        let attributes = AlertsPopUps.bottomAlertAttributes
        
        // Generate textual content
        let title = EKProperty.LabelContent(text: "Timer!", style: .init(font: MainFont.medium.with(size: 15), color: .black, alignment: .center))
        let description = EKProperty.LabelContent(text: "Please Select search repeat Timer. Search will be Conducted on Auto Repeat.", style: .init(font: MainFont.light.with(size: 13), color: .black, alignment: .center))
        let image = EKProperty.ImageContent(imageName: "whiteTick", size: CGSize(width: 25, height: 25), contentMode: .scaleAspectFit)
        let simpleMessage = EKSimpleMessage(image: image, title: title, description: description)
        
        // Generate buttons content
        let buttonFont = MainFont.medium.with(size: 16)
        
        let buttonLabelStyle = EKProperty.LabelStyle(font: buttonFont, color: EKColor.Teal.a600)
        var buttonLabel = EKProperty.LabelContent(text: "10 Seconds", style: buttonLabelStyle)
        let TenButton = EKProperty.ButtonContent(label: buttonLabel, backgroundColor: .clear, highlightedBackgroundColor:  EKColor.Teal.a600.withAlphaComponent(0.05)) {
            SwiftEntryKit.dismiss()
            self.timerValue = 10
            self.timerBtn.setTitle("10 seconds", for: .normal)
        }
        
        buttonLabel.text = "20 Seconds"
        let TwentyButton = EKProperty.ButtonContent(label: buttonLabel, backgroundColor: .clear, highlightedBackgroundColor:  EKColor.Teal.a600.withAlphaComponent(0.05)) {
            SwiftEntryKit.dismiss()
            self.timerValue = 20
            self.timerBtn.setTitle("20 seconds", for: .normal)
        }
        
        buttonLabel.text = "30 Seconds"
        let ThirtyButton = EKProperty.ButtonContent(label: buttonLabel, backgroundColor: .clear, highlightedBackgroundColor:  EKColor.Teal.a600.withAlphaComponent(0.05)) {
            SwiftEntryKit.dismiss()
            self.timerValue = 30
            self.timerBtn.setTitle("30 seconds", for: .normal)
        }
        
        buttonLabel.text = "40 Seconds"
        let fortyButton = EKProperty.ButtonContent(label: buttonLabel, backgroundColor: .clear, highlightedBackgroundColor:  EKColor.Teal.a600.withAlphaComponent(0.05)) {
            SwiftEntryKit.dismiss()
            self.timerValue = 40
            self.timerBtn.setTitle("40 seconds", for: .normal)
        }
        
        buttonLabel.text = "50 Seconds"
        let fiftyButton = EKProperty.ButtonContent(label: buttonLabel, backgroundColor: .clear, highlightedBackgroundColor:  EKColor.Teal.a600.withAlphaComponent(0.05)) {
            SwiftEntryKit.dismiss()
            self.timerValue = 50
            self.timerBtn.setTitle("50 seconds", for: .normal)
        }
        
        // Generate the content
        let buttonsBarContent = EKProperty.ButtonBarContent(with: TenButton, TwentyButton, ThirtyButton, fortyButton, fiftyButton, separatorColor: EKColor.Gray.light, expandAnimatedly: true)
        
        let alertMessage = EKAlertMessage(simpleMessage: simpleMessage, buttonBarContent: buttonsBarContent)
        
        // Setup the view itself
        let contentView = EKAlertMessageView(with: alertMessage)
        
        SwiftEntryKit.display(entry: contentView, using: attributes)
    }

}

