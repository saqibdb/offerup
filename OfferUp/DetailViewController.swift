//
//  DetailViewController.swift
//  OfferUp
//
//  Created by iBuildX on 01/10/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

import UIKit
import SwiftSpinner

class DetailViewController: UIViewController, UIWebViewDelegate {

    
    @IBOutlet weak var oldWebView: UIWebView!
    
    var selectedCar : OfferUpCarModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        oldWebView.delegate = self
        let requestStr = "\(self.selectedCar.item.get_full_url!)"
        
        self.loadProxiedWebView(withRequestString: requestStr)
        SwiftSpinner.show("Loading...", animated: true)
    }
    
    
    func loadProxiedWebView(withRequestString requestStr : String){
            let proxyHost = "212.129.48.100"

            let proxyPort = NSNumber(integerLiteral: 8080)
            let userName = "umer123.offerup_1"
            let password = "umer1234"
            
            
            
            let proxyDict : [String : Any] = ["HTTPSEnable" : NSNumber(integerLiteral: 1),
                                              kCFStreamPropertyHTTPSProxyHost as String: proxyHost,
                                               kCFStreamPropertyHTTPSProxyPort as String: proxyPort,
                                               
                                               "HTTPEnable" : NSNumber(integerLiteral: 1),
                                               kCFStreamPropertyHTTPProxyHost as String: proxyHost,
                                               kCFStreamPropertyHTTPProxyPort as String: proxyPort,
                                               kCFProxyUsernameKey as String: userName,
                                               kCFProxyPasswordKey as String: password,
                                               ]
            
            
            let configuration = URLSessionConfiguration.default
            configuration.connectionProxyDictionary = proxyDict
            configuration.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData

            let headers : [String : String] = ["content-type": "application/json",
            "cache-control": "no-cache",
            "postman-token": "5bc2aa35-08c9-a436-905d-5d801e18feff",
            "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36"]
        let url = URL(string: requestStr)!
        var request = URLRequest(url: url ,timeoutInterval: Double.infinity)

        
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.allHTTPHeaderFields = headers

            request.httpMethod = "GET"
            
            
            let session = URLSession(configuration: configuration, delegate: nil, delegateQueue: OperationQueue.main)
        

            let task = session.dataTask(with: request) { data, response, error in
                SwiftSpinner.hide()
              guard let data = data else {
                
                AlertsPopUps.showPopupMessage(title: "Error", titleColor: .white, description: error!.localizedDescription, descriptionColor: .white, buttonTitleColor: .darkSubText, buttonBackgroundColor: .white, image: UIImage(named: "whiteExclamation")!, type: .error)
                return
              }
            
                self.oldWebView.load(data, mimeType: "text/html", textEncodingName: "", baseURL: url)

            }

            task.resume()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        SwiftSpinner.hide()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        SwiftSpinner.hide()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    - (void)webViewDidStartLoad:(UIWebView *)webView{
    
    }
    - (void)webViewDidFinishLoad:(UIWebView *)webView{
    dispatch_async(dispatch_get_main_queue(), ^{
    [SwiftSpinner hide:nil];
    });
    }
    - (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    dispatch_async(dispatch_get_main_queue(), ^{
    [SwiftSpinner hide:nil];
    });
    }*/

}
