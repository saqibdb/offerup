//
//  CitiesViewController.swift
//  OfferUp
//
//  Created by iBuildX on 03/10/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

import UIKit

class CitiesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    @IBOutlet weak var mainTableView: UITableView!
    
    @IBOutlet weak var cancelBtn: UIButton!
    
    @IBOutlet weak var selectBtn: UIButton!
    
    @IBOutlet weak var mainSearchBar: UISearchBar!
    
    var allCities : [CityModel] = [CityModel]()
    
    var allCitiesOriginal : [CityModel] = [CityModel]()

    
    
    var selectedCities : [CityModel] = [CityModel]()
    
    

    
    var allNamesDict : [String : Any]!
    var allNamesDictTitles : [String] = [String]()
    let allAlphabets : [String] = ["A",  "B",  "C",  "D",  "E",  "F",  "G",  "H",  "I",  "J",  "K",  "L",  "M",  "N",  "O",  "P",  "Q",  "R",  "S",  "T",  "U",  "V",  "W",  "X",  "Y",  "Z"];

    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let path = Bundle.main.path(forResource: "citiesUS", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonResults = jsonResult as? [[String: Any]] {
                    for cityDict in jsonResults {
                        let city = CityModel.deserialize(from: cityDict)
                        self.allCities.append(city!)
                    }
                    self.allCities = self.allCities.sorted(by: { $0.name < $1.name })
                }
            } catch {
                
            }
        }
        
        self.allCities = self.allCities.sorted { $0.name < $1.name }
        self.allCitiesOriginal = self.allCities

        
        mainTableView.delegate = self
        mainTableView.dataSource = self
        
        createDictionaryOfArrays()

        
        
        
        mainSearchBar.delegate = self
        // Do any additional setup after loading the view.
    }
    
    func createDictionaryOfArrays() {
        allNamesDict = (self.allCities as NSArray).linq_group(by: { (city) -> Any? in
            let cityName = (city as! CityModel).name! as NSString
            return cityName.substring(to: 1)
        }) as! [String : Any]
        allNamesDictTitles = Array(allNamesDict.keys).sorted()
        mainTableView.reloadData()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancelAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func selectAction(_ sender: UIButton) {
        self.dismiss(animated: true) {
            if UIApplication.topViewController() is ViewController {
                let topVC = UIApplication.topViewController() as! ViewController
                topVC.selectedCities = self.selectedCities
                topVC.citiesBtn.setTitle("\(self.selectedCities.count) Cities", for: .normal)
                if (topVC.carSearchText.text?.count)! > 0 {
                    topVC.goAction(UIButton())
                }
            }
        }
    }
    
    
    
    
    
    
    
    
    // TableView Delegates
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return allNamesDictTitles.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return allNamesDictTitles[section]
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return allAlphabets
    }
    
    func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        return allNamesDictTitles.index(of: title)!
    }
    
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionTitle = allNamesDictTitles[section]
        let sectionAnimals = allNamesDict[sectionTitle] as! [CityModel]
        return sectionAnimals.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let simpleTableIdentifier = "SimpleTableItem"
        var cell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: simpleTableIdentifier)
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: simpleTableIdentifier)
        }
        
        
        let sectionTitle = allNamesDictTitles[indexPath.section]
        let sectionAnimals = allNamesDict[sectionTitle] as! [CityModel]
        
        let city = sectionAnimals[indexPath.row]
        
        
        
        
        
        cell.textLabel?.text = city.name!
        cell.accessoryType = .none
        
        if selectedCities.contains(where: {($0.name == city.name)}){
            cell.accessoryType = .checkmark
        }
        tableView.allowsSelection = true
        return cell
    }
    
    
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let sectionTitle = allNamesDictTitles[indexPath.section]
        let sectionAnimals = allNamesDict[sectionTitle] as! [CityModel]
        let city = sectionAnimals[indexPath.row]
        let cell = tableView.cellForRow(at: indexPath)
        if selectedCities.contains(where: {($0.name == city.name)}){
            cell?.accessoryType = .none
            selectedCities.remove(at: selectedCities.index(where: {($0.name == city.name)})!)
        }
        else{
            cell?.accessoryType = .checkmark
            selectedCities.append(city)
        }
        
    }
    
    // MARK:- Search Bar Delegate
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count == 0 {
            self.allCities = self.allCitiesOriginal
        }
        else {
            self.allCities.removeAll()
            for city in self.allCitiesOriginal {
                if city.name.lowercased().contains(searchText.lowercased()) == true {
                    self.allCities.append(city)
                }
            }
        }
        self.createDictionaryOfArrays()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }

}
