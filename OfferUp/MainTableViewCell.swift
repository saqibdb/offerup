//
//  MainTableViewCell.swift
//  OfferUp
//
//  Created by iBuildX on 28/09/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

import UIKit
import CCMBorderView

class MainTableViewCell: UITableViewCell {

    @IBOutlet weak var newlyListedText: UILabel!
    
    @IBOutlet weak var makeText: UILabel!
    
    @IBOutlet weak var modelText: UILabel!
    
    @IBOutlet weak var priceText: UILabel!
    
    @IBOutlet weak var carNameText: UILabel!
    
    @IBOutlet weak var carTrimText: UILabel!
    
    @IBOutlet weak var mileageText: UILabel!
    
    @IBOutlet weak var phoneText: UILabel!
    
    @IBOutlet weak var emailText: UILabel!
    
    @IBOutlet weak var topIndicatorview: CCMBorderView!
    
    @IBOutlet weak var bottomIndicatorView: CCMBorderView!
    
    
    
    
    
    
    
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
